package main

import (
	"fmt"
	"os"
	"flag"
	"path/filepath"

	"bitbucket.org/kyr0s/enconv/filters"
	"bitbucket.org/kyr0s/enconv/utils/errors"
)

const filesBufferSize int = 100
const workersCount int = 10

func main() {
	configPtr := flag.String("config", "enconv.yml", "file of configuration")
	flag.Parse()
	config := *configPtr

	settings := *readSettingsFile(config)
	changedFiles:=applySettings(settings)

	for file := range changedFiles {
		fmt.Println(file)
	}
}

func createInputChannel() <-chan string {
	cout := make(chan string, filesBufferSize)

	go func() {
		defer close(cout)
		for _, file := range flag.Args() {
			fullPath, err := filepath.Abs(file)
			errors.PanicIf(err)

			cout <- fullPath
		}
	}()

	return cout
}

func readSettingsFile(config string) *ConvertSettings {
	workDir, err := os.Getwd()
	errors.PanicIf(err)

	settingsPath := filepath.Join(workDir, config)
	settings := readSettings(settingsPath)

	return settings
}

func createFilter(settings convSettings) filters.Filter {
	workDir, err := os.Getwd()
	errors.PanicIf(err)

	gitignoreFilter := filters.NewGitIgnoreFilter(settings.GitignoreSettings, workDir)

	encodingFilterFactory := func() filters.Filter {
		return filters.NewEncodingFilter(settings.EncodingSettings)
	}

	pooledEncodingFilter := filters.NewPooledFilter(encodingFilterFactory, workersCount, filesBufferSize)

	compositeFilter := filters.NewCompositeFilter(gitignoreFilter, pooledEncodingFilter)

	return compositeFilter
}

func applySettings(settings ConvertSettings) <-chan string {
	cout := make(chan string, filesBufferSize)

	go func() {
		defer close(cout)
		for _, settingsItem := range settings.ConvertSettings {
			inputFiles := createInputChannel()
			filter := createFilter(settingsItem)
			changedFiles := filter.Filter(inputFiles)

			for changedFile := range changedFiles {
				cout <- changedFile
			}
		}
	}()

	return cout
}