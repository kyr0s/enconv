package main

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"

	"bitbucket.org/kyr0s/enconv/filters"
	"bitbucket.org/kyr0s/enconv/utils/errors"
)

type convSettings struct {
	filters.GitignoreSettings `yaml:",inline"`
	filters.EncodingSettings  `yaml:",inline"`
}

type ConvertSettings struct {
	ConvertSettings []convSettings `convertSettings`
}

func readSettings(path string) *ConvertSettings {
	file, err := ioutil.ReadFile(path)
	errors.PanicIf(err)

	settings := &ConvertSettings{}
	err = yaml.Unmarshal(file, settings)
	errors.PanicIf(err)

	for _, convertSettings := range settings.ConvertSettings {
		convertSettings.Validate()
	}

	return settings
}
