package ioseq

import (
	"fmt"
	"io"
	"os"
	"bitbucket.org/kyr0s/enconv/utils/errors"
)

// Sequence provides unidirectional cached access to underlying io.ReadCloser
type Sequence struct {
	reader io.ReadCloser
	data   []byte
	buf    []byte
	blockLen int
	eof bool
}

// Forget flush first "count" bytes of cached data
func (seq *Sequence) Forget(count int) error {
	if count > len(seq.data) {
		return fmt.Errorf("Can't skip more than %v bytes currently present in sequence. Requested skip %v bytes", len(seq.data), count)
	}

	if count <= 0 {
		return fmt.Errorf("count value must be greater than zero. Requested skip %v bytes", count)
	}

	seq.data = seq.data[count:]
	return nil
}

// Data returns currently cached part of underlying io.ReadCloser
func (seq *Sequence) Data() ([]byte, bool) {
	for len(seq.data) < seq.blockLen && !seq.eof {
		count, err := seq.reader.Read(seq.buf)

		if seq.eof = err == io.EOF; !seq.eof {
			errors.PanicIf(err)
		}

		seq.data = appendPart(seq.data, seq.buf, count)
	}

	var blockLen int
	if len(seq.data) > seq.blockLen {
		blockLen = seq.blockLen
	} else {
		blockLen = len(seq.data)
	}

	data := make([]byte, 0, blockLen)
	for i := 0; i < blockLen; i++ {
		data = append(data, seq.data[i])
	}

	hasMore := !seq.eof || len(seq.data) > seq.blockLen
	return data, hasMore
}

func appendPart(target []byte, data []byte, count int) []byte {
	if count == 0 {
		return target
	}

	result := target
	if cap(target) < len(target) + count {
		result = make([]byte, 0, len(target) + count)
		for _, item := range target {
			result = append(result, item)
		}
	}

	for i := 0; i < count; i++ {
		result = append(result, data[i])
	}

	return result
}

// Close implements io.Closer.Close()
// Close affects underlying io.ReadCloser
func (seq *Sequence) Close() error {
	return seq.reader.Close()
}

// NewFromFile creates new bytes sequence from file on filesystem with user-defined block length
func NewFromFile(filePath string, blockLen int) *Sequence {
	file, err := os.Open(filePath)
	errors.PanicIf(err)

	return NewFromReader(file, blockLen)
}

// NewFromReader creates new bytes sequence from external io.ReadCloser with user-defined block length
func NewFromReader(reader io.ReadCloser, blockLen int) *Sequence {
	return &Sequence {
		buf: make([]byte, blockLen),
		data: make([]byte, 0, blockLen),
		reader: reader,
		blockLen: blockLen,
		eof: false,
	}
}