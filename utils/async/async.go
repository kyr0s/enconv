package async

// MultiplexString combines messages from multiple input channels to one output channel
func MultiplexString(cin []<-chan string, cout chan<- string) {
	counter := make(chan struct{})

	go func(n int) {
		defer close(cout)
		defer close(counter)

		for i := 0; i < n; i++ {
			<-counter
		}
	}(len(cin))

	for _, ch := range cin {
		go func(src <-chan string) {
			defer func() { counter <- struct{}{} }()

			for message := range src {
				cout <- message
			}
		}(ch)
	}
}

// PoolString delegates work to wokrkers() pool
func PoolString(cin <-chan string, cout chan<- string, worker func(<-chan string) <-chan string, poolSize int) {
	workerChannels := make([]<-chan string, poolSize)

	for w := 0; w < poolSize; w++ {
		workerChannels[w] = worker(cin)
	}

	MultiplexString(workerChannels, cout)
}