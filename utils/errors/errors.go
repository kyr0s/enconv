package errors

import "fmt"

// PanicIf error is not nil
func PanicIf(err error) {
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
}