package filters

import (
	"fmt"
	"golang.org/x/text/encoding/charmap"
	"io"
	"io/ioutil"
	"math/bits"
	"os"

	"bitbucket.org/kyr0s/enconv/utils/errors"
	"bitbucket.org/kyr0s/enconv/utils/ioseq"
	"github.com/nu7hatch/gouuid"
)

const (
	actionNone int = iota
	actionAddBom
	actionRemoveBom
	actionConvert
)

var bom = []byte{0xEF, 0xBB, 0xBF}

// EncodingFilter test file encoding is UTF-8 and converts it from expected source encoding if not
type EncodingFilter struct {
	charmap    *charmap.Charmap
	insertBOM  bool
}

// Filter implements Filter.Filter().
// Filter tests file encoding is correct UTF-8 and convert encoding from expected source if not.
// Only converted files returned in output channel
func (filter *EncodingFilter) Filter(files <-chan string) <-chan string {
	cout := make(chan string)

	go func() {
		defer close(cout)

		for file := range files {
			action := testFile(file, filter.insertBOM)

			switch action {
			case actionAddBom:
				addBOM(file)
			case actionRemoveBom:
				removeBOM(file)
			case actionConvert:
				convert(file, filter.insertBOM, filter.charmap)
			}

			if action != actionNone {
				cout <- file
			}
		}
	}()

	return cout
}

func testFile(filePath string, insertBOM bool) int {
	seq := ioseq.NewFromFile(filePath, 4)
	defer seq.Close()

	buf, more := seq.Data()
	if !more && len(buf) == 0 {
		return actionNone
	}

	hasBom := startsWithBom(seq)
	isValid := isValidUTF8Sequence(seq)

	switch {
	case !isValid:
		return actionConvert
	case hasBom && !insertBOM:
		return actionRemoveBom
	case !hasBom && insertBOM:
		return actionAddBom
	default:
		return actionNone
	}
}

var twoBytesPattern byte = 0xC0
var threeBytesPattern byte = 0xE0
var fourBytesPattern byte = 0xF0
var tailBytesPattern byte = 0x80

func isValidUTF8Sequence(seq *ioseq.Sequence) bool {
	isValid := true
	buf, hasMore := seq.Data()
	if !hasMore && len(buf) == 0 {
		return isValid
	}

	for ; (hasMore || len(buf) > 0) && isValid; buf, hasMore = seq.Data() {
		var charLen int
		switch {
		case bits.LeadingZeros8(fourBytesPattern^buf[0]) >= 5:
			charLen = 4
		case bits.LeadingZeros8(threeBytesPattern^buf[0]) >= 4:
			charLen = 3
		case bits.LeadingZeros8(twoBytesPattern^buf[0]) >= 3:
			charLen = 2
		default:
			charLen = 1
		}

		isValid = isValidChar(buf, charLen)
		if isValid {
			seq.Forget(charLen)
		}
	}

	return isValid
}

func isValidChar(buf []byte, charLen int) bool {
	if len(buf) < charLen {
		return false
	}

	for i := 1; i < charLen; i++ {
		if bits.LeadingZeros8(tailBytesPattern^buf[i]) < 2 {
			return false
		}
	}

	return true
}

func startsWithBom(seq *ioseq.Sequence) bool {
	buf, _ := seq.Data()

	if len(buf) < len(bom) {
		return false
	}

	for i := 0; i < len(bom); i++ {
		if buf[i] != bom[i] {
			return false
		}
	}

	seq.Forget(len(bom))
	return true
}

func addBOM(sourceFilePath string) {
	transformation := func(sourceFile io.Reader, tempFile io.Writer) {
		_, err := tempFile.Write(bom)
		errors.PanicIf(err)

		_, err = io.Copy(tempFile, sourceFile)
		errors.PanicIf(err)
	}

	transform(sourceFilePath, transformation)
}

func removeBOM(sourceFilePath string) {
	transformation := func(sourceFile io.Reader, tempFile io.Writer) {
		if s, ok := sourceFile.(io.Seeker); ok {
			_, err := s.Seek(3, os.SEEK_CUR)
			errors.PanicIf(err)
		} else {
			_, err := io.CopyN(ioutil.Discard, sourceFile, 3)
			errors.PanicIf(err)
		}

		_, err := io.Copy(tempFile, sourceFile)
		errors.PanicIf(err)
	}

	transform(sourceFilePath, transformation)
}

func convert(sourceFilePath string, insertBOM bool, sourceEncoding *charmap.Charmap) {
	transformation := func(sourceFile io.Reader, tempFile io.Writer) {
		if insertBOM {
			_, err := tempFile.Write(bom)
			errors.PanicIf(err)
		}

		reader := sourceEncoding.NewDecoder().Reader(sourceFile)
		_, err := io.Copy(tempFile, reader)
		errors.PanicIf(err)
	}

	transform(sourceFilePath, transformation)
}

func transform(sourceFilePath string, transformation func(io.Reader, io.Writer)) {
	sourceFile, err := os.Open(sourceFilePath)
	errors.PanicIf(err)
	defer sourceFile.Close()

	tempFile, tempFilePath := createTempFile(sourceFilePath)
	defer tempFile.Close()

	transformation(sourceFile, tempFile)
	sourceFile.Close()
	tempFile.Close()

	err = os.Rename(tempFilePath, sourceFilePath)
	errors.PanicIf(err)
}

func createTempFile(sourceFilePath string) (*os.File, string) {
	uuid, err := uuid.NewV4()
	errors.PanicIf(err)
	tempFilePath := fmt.Sprintf("%v.%v.tmp", sourceFilePath, uuid)
	tempFile, err := os.Create(tempFilePath)
	errors.PanicIf(err)

	return tempFile, tempFilePath
}

// EncodingSettings describes expected 8-bit encoding and BOM presence for EncodingFilter
type EncodingSettings struct {
	Encoding   string
	InsertBOM  bool   `yaml:"insertBOM"`

}

// NewEncodingFilter creates new EncodingFilter with user-defind expected 8-bit encoding
func NewEncodingFilter(settings EncodingSettings) *EncodingFilter {
	charmap := getEncoding(settings.Encoding)

	return &EncodingFilter{
		insertBOM: settings.InsertBOM,
		charmap:   charmap,
	}
}

func getEncoding(name string) *charmap.Charmap {
	switch name {
	case "win1251":
		return charmap.Windows1251
	default:
		panic("not supported encoding: " + name)
	}
}
