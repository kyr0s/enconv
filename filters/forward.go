package filters

// ForwardFilter just transfers input messages to output
type ForwardFilter struct {}

// Filter implements Filter.Filter(): just transfers input messages to output
func (filter *ForwardFilter) Filter(files <-chan string) <-chan string {
	cout := make(chan string)

	go func() {
		defer close(cout)

		for file := range files {
			cout <- file
		}
	}()

	return cout
}

// NewForwardFilter creates ForwardFilter
func NewForwardFilter() *ForwardFilter {
	return &ForwardFilter{}
}
