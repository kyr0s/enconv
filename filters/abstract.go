package filters

// Filter performs input files filtration based on user defined patterns
type Filter interface {

	// Filter performs input files filtration based on user defined patterns
	Filter(files <-chan string) <-chan string
}