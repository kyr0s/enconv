package filters

import (
	"io/ioutil"
	"path/filepath"

	"bitbucket.org/kyr0s/enconv/utils/errors"
)

// EmptyFilter recursivly scan path and returns all found files if input channel is empty
type EmptyFilter struct {
	path string
}

// Filter implements Filter.Filter()
// Filter returns files from input channel OR walk file system if input channel closed without any message
func (filter *EmptyFilter) Filter(files <-chan string) <-chan string {
	cout := make(chan string)

	go func() {
		defer close(cout)

		isEmpty := true
		for file := range files {
			isEmpty = false
			cout <- file
		}

		if isEmpty {
			dirs := []string{filter.path}

			for ; len(dirs) > 0; dirs = dirs[1:] {
				currentDir := dirs[0]

				files, err := ioutil.ReadDir(currentDir)
				errors.PanicIf(err)

				for _, file := range files {
					fullpath := filepath.Join(currentDir, file.Name())

					if file.IsDir() {
						dirs = append(dirs, fullpath)
					} else {
						cout <- fullpath
					}
				}
			}
		}
	}()

	return cout
}

// NewEmptyFilter creates EmptyFilter with specified base path to visit when input channel is empty
func NewEmptyFilter(basePath string) *EmptyFilter {
	return &EmptyFilter {
		path: basePath,
	}
}